var express = require('express'),
	http = require('http'),
	path = require('path');

var app = module.exports = express();

require('./db');

app.configure(function () {
	app.set('port', process.env.PORT || 3000);
	app.set('views', __dirname + '/views');
	app.set('view engine', 'hjs');
	app.use(express.favicon());
	app.use(express.logger('dev'));
	app.use(express.bodyParser());
	app.use(function(req, res, next) {
      res.header("Access-Control-Allow-Origin", "*");
      res.header("Access-Control-Allow-Headers", "X-Requested-With");
      next();
    });
	app.use(express.methodOverride());
	app.use(app.router);
	app.use(require('less-middleware')({ src: __dirname + '/public' }));
	app.use(express.static(path.join(__dirname, 'public')));
});

app.configure( 'development', function (){
  app.use( express.errorHandler({ dumpExceptions : true, showStack : true }));
});
 
app.configure( 'production', function (){
  app.use( express.errorHandler());
});

var routes = require('./routes');
routes.user = require('./routes/user');
routes.game = require('./routes/game');
routes.admin = require('./routes/admin');
routes.deal = require('./routes/deal');

app.get('/', routes.index);
app.get('/user', routes.user.index);
app.post('/user/login', routes.user.login);
app.post('/user/update-profile', routes.user.updateProfile);
app.get('/user/:username', routes.user.userIndex);
app.post('/game/claim', routes.game.claim);
app.get('/game/leaderboard/:limit?', routes.game.leaderboard);
app.get('/game/stats/:username', routes.game.stats);
app.get('/admin/codes', routes.admin.codes);
app.post('/admin/code/add/:id', routes.admin.addCode);
app.get('/admin/populate', routes.admin.populate);
app.get('/admin/clear-codes', routes.admin.clearCodes);
app.get('/deals/list', routes.deal.list);

app.listen(process.env.PORT || 3000);