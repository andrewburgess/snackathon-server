var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var User = new Schema({
	username: String,
	password: String,
	firstName: { type: String, default: '' },
	lastName: { type: String, default: '' },
	image: { type: String, default: 'http://snackathon.azurewebsites.net/images/none.png' },
	totalPoints: { type: Number, default: 0 },
	gameStats: {
		highScore: { type: Number, default: Math.floor(Math.random() * 100000) + 1 },
		levelsCleared: { type: Number, default: Math.floor(Math.random() * 100) + 1 },
		pointsEarned: { type: Number, default: Math.floor(Math.random() * 5000) + 1 }
	}
});

mongoose.model('User', User);

var Code = new Schema({
	code: String,
	points: Number,
	productId: Schema.Types.ObjectId
});

mongoose.model('Code', Code);

var ClaimedCode = new Schema({
	userId: Schema.Types.ObjectId,
	codeId: Schema.Types.ObjectId,
	dateClaimed: { type: Date, default: Date.now }
});

mongoose.model('ClaimedCode', ClaimedCode);

var Product = new Schema({
	name: String,
	image: String
});

mongoose.model('Product', Product);

var Deal = new Schema({
	name: String,
	image: String,
	points: Number,
	expiration: Date,
	location: String
});

mongoose.model('Deal', Deal);

mongoose.connect('mongodb://snackathon:snackathon@ds027748.mongolab.com:27748/snackathondb', {
	server: { auto_reconnect: true,
		poolSize: 10,
		socketOptions: {
			connectTimeoutMS: 1800000, 
			keepAlive: 1, 
			socketTimeoutMS: 1800000
		}
	},
	db: {
		numberOfRetries: 10,
		retryMilliSeconds: 1000
	}
});