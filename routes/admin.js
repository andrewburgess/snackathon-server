var mongoose = require('mongoose'),
	q		 = require('q');

var Code = mongoose.model('Code');
var ClaimedCode = mongoose.model('ClaimedCode');
var User = mongoose.model('User');
var Product = mongoose.model('Product');
var Deal = mongoose.model('Deal');

exports.codes = function (req, res) {
	Code.find().exec().then(function (codes) {
		res.json(codes);
	}).then(null, function(err) {
		console.error(err);
		res.json({
			success: false,
			message: err.message
		});
	});
};

exports.addCode = function (req, res) {
	var code = new Code({
		points: req.body.points,
		code: req.params.id,
		unique: req.body.unique || false
	});

	code.save(function (err, savedCode) {
		if (err) {
			res.json({
				success: false,
				message: err.message
			});
			return;
		}

		res.json({
			success: true
		});
	});
};

exports.clearCodes = function (req, res) {
	ClaimedCode.find().exec().then(function (codes) {
		var tasks = [];

		for (var i = 0; i < codes.length; i++) {
			tasks.push(codes[i].remove());
		}

		q.all(tasks).when(function () {
			res.json({ success: true });
		})
	});
}

exports.populate = function (req, res) {
	/*var oreos = new Product({
		name: 'Oreos',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg'
	});

	var chips = new Product({
		name: 'Chips Ahoy',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg'
	});

	var bel = new Product({
		name: 'Belvita',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg'
	});

	var tasks = [];
	tasks.push(oreos.save());
	tasks.push(chips.save());
	tasks.push(bel.save());

	q.all(tasks).when(function () {
		tasks = [];
		tasks.push(new Code({ code: 'abc', points: 50, productId: oreos._id }).save());
		tasks.push(new Code({ code: 'def', points: 75, productId: oreos._id }).save());
		tasks.push(new Code({ code: 'ghi', points: 150, productId: chips._id }).save());
		tasks.push(new Code({ code: 'jkl', points: 10, productId: bel._id }).save());

		q.all(tasks).when(function () {
			res.json({ success: true });
		})
	});*/

	var oreos = new Deal({
		name: 'Oreos Buy One Get One Free',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg',
		points: 300,
		expiration: new Date(2013, 8, 2, 0, 0, 0),
		location: 'Wegmans'
	});

	var chips = new Deal({
		name: 'Chips Ahoy $1 Off',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg',
		points: 200,
		expiration: new Date(2013, 9, 1, 0, 0, 0),
		location: 'CVS'
	});

	var bel = new Deal({
		name: 'Stride Gum Multipacks',
		image: 'http://4.bp.blogspot.com/-AkcJivMGhHA/TpLz1c6XGGI/AAAAAAAAAlg/KRldkJZhqec/s400/oreo1.jpg',
		points: 75,
		expiration: new Date(2014, 1, 1, 0, 0, 0),
		location: 'Major Supermarkers'
	});

	var tasks = [];
	tasks.push(oreos.save());
	tasks.push(chips.save());
	tasks.push(bel.save());

	q.all(tasks).when(function () {
		res.json({ success: true });
	});
}