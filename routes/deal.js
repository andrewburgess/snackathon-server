var mongoose = require('mongoose');

var Deal = mongoose.model('Deal');

exports.list = function (req, res) {
	Deal.find().exec().then(function (deals) {
		res.json(deals);
	}).then(null, function (err) {
		console.error(err);
		res.json({
			success: false,
			message: err.message
		});
	})
}