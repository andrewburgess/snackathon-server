var mongoose = require('mongoose');
var q = require('q');

var Code = mongoose.model('Code');
var ClaimedCode = mongoose.model('ClaimedCode');
var User = mongoose.model('User');
var Product = mongoose.model('Product');

function findUser(username) {
	return User.findOne({ username: username }).exec();
}

function findCode(code) {
	return Code.findOne({ code: code }).exec();
}

function findClaimedCodes(userId, codeId) {
	return ClaimedCode.find({ 
		userId: userId, 
		codeId: codeId
	}).exec();
}

function CodeNotFoundException() {
	this.name = 'CodeNotFoundException';
	this.message = 'Code was not found';
	this.code = 1
}

CodeNotFoundException.prototype = Error.prototype;

function CodeAlreadyClaimedException() {
	this.name = 'CodeAlreadyClaimedException';
	this.message = 'Code was already claimed!';
	this.code = 2;
}

CodeAlreadyClaimedException.prototype = Error.prototype;

exports.claim = function (req, res) {
	var self = this;

	console.log('claim');
	findUser(req.body.username).then(function (user) {
		console.log(user);
		if (!user) {
			throw new Error('User was not found!');
		}

		self.user = user;

		return findCode(req.body.code);
	}).then(function (code) {
		console.log(code);
		if (!code) {
			throw new CodeNotFoundException();
		}

		self.code = code;

		return findClaimedCodes(self.user._id, self.code._id);
	}).then(function (claimedCodes) {
		console.log(claimedCodes);
		if (claimedCodes && claimedCodes.length > 0)
			throw new CodeAlreadyClaimedException();

		//User hasn't claimed, insert a new claimed code
		var claimed = new ClaimedCode({
			userId: self.user._id,
			codeId: self.code._id
		});

		var tasks = [];
		tasks.push(claimed.save());
		return q.all(tasks);
	}).then(function () {
		console.log('find');
		return ClaimedCode.find({ userId: self.user._id }, 'codeId').exec();
	}).then(function (claimed) {
		console.log(claimed);

		var ids = [];
		for (var i = 0; i < claimed.length; i++) {
			ids.push(claimed[i].codeId);
		}

		return Code.find({
			_id: { $in: ids }
		}, 'points').exec();
	}).then(function (allPoints) {
		console.log(allPoints);
		var total = 0;
		for (var i = 0; i < allPoints.length; i++) {
			total += allPoints[i].points;
		}

		self.user.totalPoints = total;
		self.user.save();

		console.log(total);
		res.json({
			success: true,
			points: self.code.points,
			totalPoints: total
		});
		return;
	}).then(null, function (err) {
		console.error(err);
		res.json({
			success: false,
			message: err.message,
			code: err['code'] || -1
		});

		return;
	});
}

exports.leaderboard = function (req, res) {
	User.find().sort('-totalPoints').limit(req.params.limit || 5).
		select('username totalPoints image firstName lastName').lean().exec().then(function (userList) {
			for (var i = 0; i < userList.length; i++) {
				userList[i]['rank'] = i + 1;
			}
			res.json(userList);
	}).then(null, function (err) {
		console.error(err);
		res.json({
			success: false,
			message: err.message
		});
	})
}

exports.stats = function (req, res) {
	var self = this;

	User.findOne({ username: req.params.username}).exec().then(function (user) {
		console.log(user);
		self.user = user;

		return ClaimedCode.find({ userId: self.user._id }, 'codeId').exec();
	}).then(function (claimedCodes) {
		console.log(claimedCodes);
		self.claimedCodes = claimedCodes;

		var codeIds = [];
		for (var i = 0; i < claimedCodes.length; i++) {
			codeIds.push(claimedCodes[i].codeId);
		}

		console.log(codeIds);
		return Code.find({ _id: { $in: codeIds } }).exec();
	}).then(function (codes) {
		console.log(codes);
		self.codes = codes;

		var productIds = [];
		for (var i = 0; i < codes.length; i++) {
			productIds.push(codes[i].productId);
		}

		return Product.find({ _id: { $in: productIds } }).exec();
	}).then(function (products) {
		console.log(products);
		console.log('go');
		self.products = products;

		productStats = [];

		for (var i = 0; i < products.length; i++) {
			var prod = products[i];
			console.log(prod);
			var codes = self.codes.filter(function (el, index, arr) {
				console.log(el);
				return el.productId.toString() === prod._id.toString();
			});

			productStats.push({
				name: prod.name,
				count: codes.length,
				image: prod.image
			});
		}

		productStats.sort(function (a, b) { 
			return a.count < b.count;
		});

		var ret = {
			productStats: productStats,
			gameStats: self.user.gameStats
		}

		res.json(ret);
	});
}