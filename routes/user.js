var mongoose = require('mongoose');

var User = mongoose.model('User');

function findUser(username) {
	return User.findOne({ username: username }).exec();
}

function createUser(userData, callback) {
	var user = new User({ username: userData.username, password: userData.password });

	return user.save(callback);
}

exports.index = function (req, res) {
	var promise = findUser('andrew');
	promise.then(function (user) {
		if (user)
			res.json(user);
		else {
			res.send('did not find user');
		}
	});
}

exports.userIndex = function (req, res) {
	User.findOne({ username: req.params.username }).select('username firstName lastName image totalPoints').exec().then(function (user) {
		if (user)
			res.json(user);
		else {
			res.send('did not find user');
		}
	});
}

exports.login = function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "X-Requested-With");

	var promise = findUser(req.body.username);
	promise.then(function (user) {
		if (user) {
			res.json({
				success: true
			});
		} else {
			createUser(req.body, function (err, user) {
			 	console.log('created');
			 	findUser(req.body.username).then(function (user) {
					if (user) {
						console.log('found');
						res.json({
							success: true,
							created: true
						});
					} else {
						console.log('not found');
						res.json({
							success: false,
							message: 'something went wrong'
						});
					}
				});
			 });
		}
	});
}

exports.updateProfile = function (req, res) {
	User.findOne({ username: req.body.username }).exec().then(function (user) {
		console.log(user);
		user.firstName = req.body.firstName;
		user.lastName = req.body.lastName;
		user.image = req.body.image;

		user.save(function (err) {
			if (err) {
				res.json({
					success: false,
					message: err.message
				});
			} else {
				res.json({
					success: true
				});
			}
		});
	})
};